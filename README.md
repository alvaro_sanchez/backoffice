# Grails and AngularJS Demo

Welcome! This project is a demo of a Single Page Application created using:

* AngularJS for a pure HTML/JS front-end application (`frontend` folder), using Angular's `$resource` to communicate with the server. This project can be built and run using Yeoman (Yo, Grunt and Bower).
* Grails for the backend (`backend` folder), using Grails 2.3.1 excellent capabilities for serving a REST API. The application is meant to be used in standalone mode (with Jetty embedded)

This is the companion code of my talk "[Developing SPI applications using Grails and AngularJS](http://www.slideshare.net/alvarosanchezmariscal/codemotion2013)".

## How to get started

1. Install [Yeoman](http://yeoman.io) (note: you don't need Grails. It will be downloaded automatically) and [Compass](http://compass-style.org/):

        npm install -g yo
        sudo gem install compass

2. Make sure you have `yo`, `grunt` and `bower` command line tools:

        yo --version
        grunt --version
        bower --version

3. Start the backend:

        cd backend
        ./grailsw compile && ./grailsw build-standalone
        java -jar target/standalone-0.1.jar

4. Install frontend dependencies (you need to do this only once):

        cd ../frontend
        npm install
        bower update

5. Start the frontend:

        grunt server

## Roadmap

* Setup API security using a token based strategy. Useful links regarding this:
    * [http://www.jamesward.com/2013/05/13/securing-single-page-apps-and-rest-services](http://www.jamesward.com/2013/05/13/securing-single-page-apps-and-rest-services)
    * [http://www.webdeveasy.com/single-page-application-authentication/](http://www.webdeveasy.com/single-page-application-authentication/)
    * [http://frederiknakstad.com/authentication-in-single-page-applications-with-angular-js/](http://frederiknakstad.com/authentication-in-single-page-applications-with-angular-js/)
    * [http://arthur.gonigberg.com/2013/06/29/angularjs-role-based-auth/](http://arthur.gonigberg.com/2013/06/29/angularjs-role-based-auth/)
    * [http://glue-talk.com/1509/angular-js-authentication-for-webapplication-and-webservice/](http://glue-talk.com/1509/angular-js-authentication-for-webapplication-and-webservice/)
    * [https://coderwall.com/p/f6brkg](https://coderwall.com/p/f6brkg)
    * [http://blog.brunoscopelliti.com/authentication-to-a-restful-web-service-in-an-angularjs-web-app](http://blog.brunoscopelliti.com/authentication-to-a-restful-web-service-in-an-angularjs-web-app)
* Setup API documentation using [Swagger](https://github.com/alvarosanchez/grails-swagger).
